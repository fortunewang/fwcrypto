PROJECT(fwcrypto C)
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

OPTION(MONOLITHIC "Link all features into a monolithic library." OFF)
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Os -std=c99")

if(MONOLITHIC)
    ADD_LIBRARY(fwc SHARED aes.c twofish.c)
else()
    ADD_LIBRARY(aes SHARED aes.c)
    SET_PROPERTY(TARGET aes PROPERTY PREFIX "")
    ADD_LIBRARY(twofish SHARED twofish.c)
    SET_PROPERTY(TARGET twofish PROPERTY PREFIX "")
endif()