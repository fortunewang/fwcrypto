import os
import cffi

__all__ = []

ffi = cffi.FFI()

if os.name == 'nt':
    suffix = '.dll'
elif os.name == 'posix':
    suffix = '.so'
else:
    raise ImportError('Unsupported OS.')


def load_library(name, additional_paths):
    try:
        lib = ffi.dlopen(name + suffix)
    except OSError as e:
        lib = None
    if lib is not None:
        return lib
    for path in additional_paths:
        try:
            lib = ffi.dlopen(path + os.sep + name + suffix)
        except OSError as e:
            lib = None
        if lib is not None:
            return lib
    raise OSError('Library not found: {}'.format(name))
