from .base import ffi, BlockCipherModeHasIV


class CBC(BlockCipherModeHasIV):
    __slots__ = []

    def _encrypt_impl(self, buf):
        assert self._vec
        assert len(buf) % self._algo.block_size == 0
        for i in range(0, len(buf), self._algo.block_size):
            curbuf = buf + i
            # vec = PT ^ VEC
            for j in range(self._algo.block_size):
                self._vec[j] ^= curbuf[j]
            # vec = enc(vec) ==> enc(PT ^ VEC)
            self._algo._encblocks_c(
                self._vec, self._algo.block_size, self._vec
            )
            # ct = vec
            curbuf[0:self._algo.block_size] = \
                self._vec[0:self._algo.block_size]

    def _decrypt_impl(self, buf):
        assert self._vec
        assert len(buf) % self._algo.block_size == 0
        tmpbuf = ffi.new('uint8_t[{}]'.format(self._algo.block_size))
        for i in range(0, len(buf), self._algo.block_size):
            curbuf = buf + i
            # tmp = dec(CT)
            self._algo._decblocks_c(curbuf, self._algo.block_size, tmpbuf)
            # tmp ^= VEC ==> dec(CT) ^ VEC
            for j in range(self._algo.block_size):
                tmpbuf[j] ^= self._vec[j]
            # vec = CT
            self._vec[0:self._algo.block_size] = \
                curbuf[0:self._algo.block_size]
            # pt = tmp
            curbuf[0:self._algo.block_size] = \
                tmpbuf[0:self._algo.block_size]
