from ....core import ffi
from ..algorithms.base import BaseBlockCipher


class BaseBlockCipherMode:
    __slots__ = ['_algo']

    def __init__(self, algo):
        assert isinstance(algo, BaseBlockCipher)
        self._algo = algo

    def _encrypt_impl(self, buf):
        pass

    def _decrypt_impl(self, buf):
        pass

    def encrypt(self, plain_text: 'bytes') -> 'bytes':
        assert isinstance(plain_text, bytes)
        if not plain_text:
            return b''
        buf = ffi.new('uint8_t[{}]'.format(len(plain_text)), plain_text)
        self._encrypt_impl(buf)
        return ffi.buffer(buf)[:]

    def decrypt(self, cipher_text: 'bytes') -> 'bytes':
        assert isinstance(cipher_text, bytes)
        if not cipher_text:
            return b''
        buf = ffi.new('uint8_t[{}]'.format(len(cipher_text)), cipher_text)
        self._decrypt_impl(buf)
        return ffi.buffer(buf)[:]


class BlockCipherModeHasIV(BaseBlockCipherMode):
    __slots__ = ['_iv', '_vec']

    def __init__(self, algo):
        assert isinstance(algo, BaseBlockCipher)
        self._algo = algo
        self._iv = None
        self.reset()

    def _get_iv(self):
        return self._iv

    def _set_iv(self, value):
        if value is None:
            self._iv = None
            return
        assert isinstance(value, bytes)
        assert len(value) == self._algo.block_size
        self._iv = value
        self.reset()

    iv = property(_get_iv, _set_iv)

    def reset(self):
        self._vec = None if self._iv is None else ffi.new(
            'uint8_t[{}]'.format(self._algo.block_size),
            self._iv
        )
