from .base import BlockCipherModeHasIV


class OFB(BlockCipherModeHasIV):
    __slots__ = []

    def _xccrypt_impl(self, buf):
        assert self._vec
        buflen = len(buf)
        rest = buflen % self._algo.block_size
        if rest != 0:
            buflen -= rest
        for i in range(0, buflen, self._algo.block_size):
            curbuf = buf + i
            # vec = enc(VEC)
            self._algo._encblocks_c(
                self._vec, self._algo.block_size, self._vec
            )
            # out = in ^ vec
            for j in range(self._algo.block_size):
                curbuf[j] ^= self._vec[j]
        if rest != 0:
            curbuf = buf + buflen
            self._algo._encblocks_c(
                self._vec, self._algo.block_size, self._vec
            )
            for j in range(rest):
                curbuf[j] ^= self._vec[j]
            self.reset()

    def _encrypt_impl(self, buf):
        self._xccrypt_impl(buf)

    def _decrypt_impl(self, buf):
        self._xccrypt_impl(buf)
