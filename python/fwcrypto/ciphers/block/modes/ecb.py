from .base import BaseBlockCipherMode


class ECB(BaseBlockCipherMode):
    __slots__ = []

    def _encrypt_impl(self, buf):
        assert len(buf) % self._algo.block_size == 0
        self._algo._encblocks_c(buf, len(buf), buf)

    def _decrypt_impl(self, buf):
        assert len(buf) % self._algo.block_size == 0
        self._algo._decblocks_c(buf, len(buf), buf)
