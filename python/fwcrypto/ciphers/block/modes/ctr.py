from .base import ffi, BlockCipherModeHasIV


class BaseCounter:
    __slots__ = ['_value']

    def __init__(self):
        self._value = None

    def set(self, value):
        self._value = None if value is None else ffi.new(
            'uint8_t[{}]'.format(len(value)), value
        )

    def inc(self):
        pass

    def get(self):
        return self._value


class LittleEndianCounter(BaseCounter):
    __slots__ = []

    def inc(self):
        if self._value is None:
            return
        for i in range(len(self._value)):
            if self._value[i] == 0xff:
                self._value[i] = 0
            else:
                self._value[i] += 1
                return


class BigEndianCounter(BaseCounter):
    __slots__ = []

    def inc(self):
        if self._value is None:
            return
        for i in range(len(self._value) - 1, -1, -1):
            if self._value[i] == 0xff:
                self._value[i] = 0
            else:
                self._value[i] += 1
                return


class CTR(BlockCipherModeHasIV):
    __slots__ = []

    def __init__(self, algo, counter=LittleEndianCounter):
        '''Constructor.

        Parameter algo is a BlockCipher object,
        who actually does the encryption/decryption.
        '''
        self._vec = counter()
        BlockCipherModeHasIV.__init__(self, algo)

    def _xccrypt_impl(self, buf):
        vecbuf = self._vec.get()
        assert vecbuf
        buflen = len(buf)
        rest = buflen % self._algo.block_size
        if rest != 0:
            buflen -= rest
        tmpbuf = ffi.new('uint8_t[{}]'.format(self._algo.block_size))
        for i in range(0, buflen, self._algo.block_size):
            curbuf = buf + i
            # tmp = enc(VEC)
            self._algo._encblocks_c(vecbuf, self._algo.block_size, tmpbuf)
            # ct = PT ^ tmp ==> PT ^ enc(VEC)
            for j in range(self._algo.block_size):
                curbuf[j] ^= tmpbuf[j]
            # vec = inc(VEC)
            self._vec.inc()
        if rest != 0:
            curbuf = buf + buflen
            self._algo._encblocks_c(vecbuf, self._algo.block_size, tmpbuf)
            for j in range(rest):
                curbuf[j] ^= tmpbuf[j]
            self.reset()

    def _encrypt_impl(self, buf):
        self._xccrypt_impl(buf)

    def _decrypt_impl(self, buf):
        self._xccrypt_impl(buf)

    def reset(self):
        '''Reset the internal vector.

        The first time user calls encrypt/decrypt,
        after calling this method,
        the internal vector will copy from IV.
        '''
        self._vec.set(self._iv)
