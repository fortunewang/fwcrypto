from .base import ffi, BlockCipherModeHasIV


class PCBC(BlockCipherModeHasIV):
    __slots__ = []

    def _encrypt_impl(self, buf):
        assert self._vec
        assert len(buf) % self._algo.block_size == 0
        for i in range(0, len(buf), self._algo.block_size):
            curbuf = buf + i
            for j in range(self._algo.block_size):
                # ct = PT ^ VEC
                curbuf[j] ^= self._vec[j]
                # vec ^= ct ==> PT
                self._vec[j] ^= curbuf[j]
            # ct = enc(ct) ==> enc(PT ^ VEC)
            self._algo._encblocks_c(curbuf, self._algo.block_size, curbuf)
            for j in range(self._algo.block_size):
                # vec ^= ct ==> PT ^ ct
                self._vec[j] ^= curbuf[j]

    def _decrypt_impl(self, buf):
        assert self._vec
        assert len(buf) % self._algo.block_size == 0
        tmpbuf = ffi.new('uint8_t[{}]'.format(self._algo.block_size))
        for i in range(0, len(buf), self._algo.block_size):
            curbuf = buf + i
            # tmp = CT
            tmpbuf[0:self._algo.block_size] = \
                curbuf[0:self._algo.block_size]
            # pt = dec(tmp) ==> dec(CT)
            self._algo._decblocks_c(tmpbuf, self._algo.block_size, curbuf)
            for j in range(self._algo.block_size):
                # pt ^= vec ==> dec(CT) ^ VEC
                curbuf[j] ^= self._vec[j]
                # vec = tmp ^ pt ==> CT ^ pt
                self._vec[j] = tmpbuf[j] ^ curbuf[j]
