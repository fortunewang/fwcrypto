from .base import ffi, BlockCipherModeHasIV


class CFB(BlockCipherModeHasIV):
    __slots__ = ['_segment_size']

    def __init__(self, algo, seg_size: 'int' = 1):
        '''Constructor.

        Parameter algo is a BlockCipher object,
        who actually does the encryption/decryption.
        '''
        BlockCipherModeHasIV.__init__(self, algo)
        self._set_segment_size(seg_size)

    def _get_segment_size(self):
        return self._segment_size

    def _set_segment_size(self, value):
        assert isinstance(value, int)
        assert 1 <= value <= self._algo.block_size
        self._segment_size = value

    segment_size = property(_get_segment_size, _set_segment_size)

    def _encrypt_impl(self, buf):
        assert self._vec
        buflen = len(buf)
        rest = buflen % self._segment_size
        if rest != 0:
            buflen -= rest
        tmpbuf = ffi.new('uint8_t[{}]'.format(self._algo.block_size))
        for i in range(0, buflen, self._segment_size):
            curbuf = buf + i
            # tmp = enc(VEC)
            self._algo._encblocks_c(self._vec, self._algo.block_size, tmpbuf)
            # ct[0:SEG] = tmp[0:SEG] ^ PT[0:SEG]
            #   ==> enc(VEC)[0:SEG] ^ PT[0:SEG]
            for j in range(self._segment_size):
                curbuf[j] ^= tmpbuf[j]
            # vec[0:BS-SEG] = VEC[SEG:BS]
            for j in range(self._segment_size, self._algo.block_size):
                self._vec[j - self._segment_size] = self._vec[j]
            # vec[BS-SEG:BS] = ct[0:SEG]
            k = self._algo.block_size - self._segment_size
            self._vec[k:self._algo.block_size] = \
                curbuf[0:self._segment_size]
        if rest != 0:
            curbuf = buf + buflen
            self._algo._encblocks_c(self._vec, self._algo.block_size, tmpbuf)
            for j in range(rest):
                curbuf[j] ^= tmpbuf[j]
            self.reset()

    def _decrypt_impl(self, buf):
        assert self._vec
        buflen = len(buf)
        rest = buflen % self._segment_size
        if rest != 0:
            buflen -= rest
        tmpbuf = ffi.new('uint8_t[{}]'.format(self._algo.block_size))
        for i in range(0, buflen, self._segment_size):
            curbuf = buf + i
            # tmp = enc(VEC)
            self._algo._encblocks_c(self._vec, self._algo.block_size, tmpbuf)
            j = 0
            # pt[0:SEG] = tmp[0:SEG] ^ CT[0:SEG]
            #   ==> enc(VEC)[0:SEG] ^ CT[0:SEG]
            # tmp[0:SEG] ^= pt[0:SEG] ==> CT[0:SEG]
            for j in range(self._segment_size):
                curbuf[j] = tmpbuf[j] ^ curbuf[j]
                tmpbuf[j] ^= curbuf[j]
            # vec[0:BS-SEG] = VEC[BS-SEG:BS]
            for j in range(self._segment_size, self._algo.block_size):
                self._vec[j - self._segment_size] = self._vec[j]
            # vec[BS-SEG:BS] = tmpbuf[0:SEG] ==> CT[0:SEG]
            k = self._algo.block_size - self._segment_size
            self._vec[k:self._algo.block_size] = \
                tmpbuf[0:self._segment_size]
        if rest != 0:
            curbuf = buf + buflen
            self._algo._encblocks_c(self._vec, self._algo.block_size, tmpbuf)
            for j in range(rest):
                curbuf[j] = tmpbuf[j] ^ curbuf[j]
            self.reset()
