from .base import ffi, BasePadder


class PKCS_7(BasePadder):
    __slots__ = []

    @classmethod
    def _paddata(cls, padlen):
        return padlen.to_bytes(1, 'little') * padlen

    @classmethod
    def _unpadlen(cls, data, block_size):
        padlen = data[-1]
        assert padlen
        assert block_size is None or padlen <= block_size
        assert padlen <= len(data)
        assert all((data[i] == padlen for i in range(-padlen, -1)))
        return padlen
