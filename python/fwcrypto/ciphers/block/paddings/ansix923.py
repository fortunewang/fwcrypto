from .base import ffi, BasePadder


class ANSI_X_923(BasePadder):
    @classmethod
    def _paddata(cls, padlen):
        return b'\x00' * (padlen - 1) + padlen.to_bytes(1, 'little')

    @classmethod
    def _unpadlen(cls, data, block_size):
        padlen = data[-1]
        assert padlen
        assert block_size is None or padlen <= block_size
        assert padlen <= len(data)
        assert all((data[i] == 0 for i in range(-padlen, -1)))
        return padlen
