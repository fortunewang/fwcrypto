from ....core import ffi


class BasePadder:
    __slots__ = []

    @classmethod
    def _padlen(cls, datalen: 'int', block_size: 'int') -> 'int':
        return block_size - datalen % block_size

    @classmethod
    def _paddata(cls, padlen: 'int') -> 'bytes':
        pass

    @classmethod
    def pad(cls, data: 'bytes', block_size: 'int') -> 'bytes':
        assert isinstance(data, bytes)
        assert isinstance(block_size, int)
        assert 1 <= block_size <= 255
        padlen = cls._padlen(len(data), block_size)
        return data + cls._paddata(padlen)

    @classmethod
    def _unpadlen(cls, data: 'bytes', block_size: 'int') -> 'int':
        pass

    @classmethod
    def unpad(cls, data: 'bytes', block_size: 'int' = None) -> 'bytes':
        assert block_size is None or (
            isinstance(block_size, int) and 1 <= block_size <= 255
        )
        assert isinstance(data, bytes) and data
        padlen = cls._unpadlen(data, block_size)
        return data[:-padlen]
