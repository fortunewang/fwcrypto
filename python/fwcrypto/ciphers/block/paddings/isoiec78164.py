from .base import ffi, BasePadder


class ISO_IEC_7816_4(BasePadder):
    __slots__ = []

    @classmethod
    def _paddata(cls, padlen):
        return b'\x80' + b'\x00' * (padlen - 1)

    @classmethod
    def _unpadlen(cls, data, block_size):
        datalen = len(data)
        padpos = datalen - 1
        while padpos > 0 and data[padpos] == 0:
            padpos -= 1
        assert data[padpos] == 0x80
        assert block_size is None or datalen - padpos <= block_size
        return datalen - padpos
