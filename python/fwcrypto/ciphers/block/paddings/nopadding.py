from .base import ffi, BasePadder


class NoPadding(BasePadder):
    __slots__ = []

    @classmethod
    def _padlen(cls, datalen, block_size):
        return 0

    @classmethod
    def _paddata(cls, padlen):
        return b''

    @classmethod
    def pad(cls, data, block_size):
        # Parameter block_size is ignored
        assert isinstance(data, bytes)
        return data

    @classmethod
    def _unpadlen(cls, data, block_size):
        return 0

    @classmethod
    def unpad(cls, data, block_size=None):
        # Parameter block_size is ignored
        assert isinstance(data, bytes)
        return data
