import os
from .base import ffi, BasePadder


class ISO_10126(BasePadder):
    __slots__ = []

    @classmethod
    def _paddata(cls, padlen):
        return os.urandom(padlen - 1) + padlen.to_bytes(1, 'little')

    @classmethod
    def _unpadlen(cls, data, block_size):
        padlen = data[-1]
        assert padlen
        assert block_size is None or padlen <= block_size
        assert padlen <= len(data)
        return padlen
