import os
import warnings
from fwcrypto.ciphers.block.algorithms.base import BaseBlockCipher
from fwcrypto.ciphers.block.modes.base import BaseBlockCipherMode
from fwcrypto.ciphers.block.paddings.base import BasePadder
from fwcrypto.ciphers.block.modes import ECB
from fwcrypto.ciphers.block.paddings import NoPadding

__all__ = ['Buffered']

NOTHING = 0
ENCRYPTION = 1
DECRYPTION = 2

# Size of buffer
# Encryption：
# buffer:   NoPadding->[0,block_size) Padding->[0,block_size)
# padded:   NoPadding->[0,block_size) Padding->block_size
# Decryption：
# buffer:   NoPadding->(0,block_size] Padding->block_size
# unpadded: NoPadding->(0,block_size] Padding->[0,block_size)
class Buffered:
    def __init__(self, algo, mode=ECB, padding=NoPadding, **kwargs):
        assert issubclass(algo, BaseBlockCipher)
        self._algo = algo()
        if 'key' in kwargs:
            self._algo.key = kwargs['key']

        assert issubclass(mode, BaseBlockCipherMode)
        self._mode = mode(self._algo)
        if 'iv' in kwargs:
            self._mode.iv = kwargs['iv']
        if 'segment_size' in kwargs:
            self._mode.segment_size = kwargs['segment_size']

        assert issubclass(padding, BasePadder)
        self._padding = padding()

        self._buffer = bytearray(b'')
        self._operation = NOTHING

    @property
    def algo(self):
        return self._algo

    @property
    def mode(self):
        return self._mode

    @property
    def padding(self):
        return self._padding

    def encrypt(self, data):
        if self._operation == DECRYPTION:
            warnings.warn('Decryption has not finished.', RuntimeWarning)
            self.reset()
        self._operation = ENCRYPTION
        return self._xcrypt(data)

    def decrypt(self, data):
        if self._operation == ENCRYPTION:
            warnings.warn('Encryption has not finished.', RuntimeWarning)
            self.reset()
        self._operation = DECRYPTION
        return self._xcrypt(data)

    def _xcrypt(self, data):
        try:
            self._buffer.extend(data)
            if not self._buffer:
                return b''
            if hasattr(self._mode, 'segment_size'):
                tmplen = len(self._buffer) - len(self._buffer) % self._mode.segment_size
            else:
                tmplen = len(self._buffer) - len(self._buffer) % self._algo.block_size
            if self._operation == ENCRYPTION:
                tmp = self._mode.encrypt(bytes(self._buffer[:tmplen]))
            else:
                # Ensure len(buffer) > 0 while decryption
                if tmplen == len(self._buffer):
                    if hasattr(self, 'segment_size'):
                        tmplen -= self._mode.segment_size
                    else:
                        tmplen -= self._algo.block_size
                tmp = self._mode.decrypt(bytes(self._buffer[:tmplen]))
            del self._buffer[:tmplen]
            return tmp
        except:
            self.reset()
            raise

    def finalize(self):
        try:
            if self._operation == NOTHING:
                return b''
            if self._operation == ENCRYPTION:
                if hasattr(self._mode, 'segment_size'):
                    tmp = self._mode.encrypt(
                        self._padding.pad(
                            bytes(self._buffer),
                            self._mode.segment_size
                        )
                    )
                else:
                    tmp = self._mode.encrypt(
                        self._padding.pad(
                            bytes(self._buffer),
                            self._algo.block_size
                        )
                    )
            else:
                if hasattr(self._mode, 'segment_size'):
                    tmp = self._padding.unpad(
                        self._mode.decrypt(bytes(self._buffer)),
                        self._mode.segment_size
                    )
                else:
                    tmp = self._padding.unpad(
                        self._mode.decrypt(bytes(self._buffer)),
                        self._algo.block_size
                    )
            self.reset()
            return tmp
        except:
            self.reset()
            raise

    def reset(self):
        del self._buffer[:]
        self._operation = NOTHING
        if hasattr(self._mode, 'reset'):
            self._mode.reset()
