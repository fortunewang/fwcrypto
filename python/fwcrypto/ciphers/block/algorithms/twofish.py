from .base import ffi, load, BaseBlockCipher

ffi.cdef(r'''
void Twofish__key_schedule(const uint8_t *key, size_t len, uint32_t *rk,
    uint32_t *sbox0, uint32_t *sbox1,
    uint32_t *sbox2, uint32_t *sbox3);
void Twofish__encblocks(const uint8_t *buf, size_t len, uint32_t *rk,
    uint32_t *sbox0, uint32_t *sbox1,
    uint32_t *sbox2, uint32_t *sbox3, uint8_t *o_buf);
void Twofish__decblocks(const uint8_t *buf, size_t len, uint32_t *rk,
    uint32_t *sbox0, uint32_t *sbox1,
    uint32_t *sbox2, uint32_t *sbox3, uint8_t *o_buf);
''')
tflib = None


class Twofish(BaseBlockCipher):
    __slots__ = ['_sbox0', '_sbox1', '_sbox2', '_sbox3', '_rk']

    def __init__(self):
        global tflib
        if tflib is None:
            tflib = load('twofish')
        self._key = None
        self._rk = None
        self._sbox0 = ffi.new('uint32_t[256]')
        self._sbox1 = ffi.new('uint32_t[256]')
        self._sbox2 = ffi.new('uint32_t[256]')
        self._sbox3 = ffi.new('uint32_t[256]')

    def _get_block_size_impl(self):
        return 16

    def _set_key_impl(self, value):
        old_key = self._key
        BaseBlockCipher._set_key_impl(self, value)
        try:
            assert len(value) in [16, 24, 32]
        except:
            self._key = old_key
            raise
        self._rk = None

    def _key_schedule(self):
        assert self._key
        self._rk = ffi.new('uint32_t[40]')
        tflib.Twofish__key_schedule(
            self._key, len(self._key), self._rk,
            self._sbox0, self._sbox1, self._sbox2, self._sbox3
        )

    def _encblocks_c(self, buf, buflen, o_buf):
        if self._rk is None:
            self._key_schedule()
        tflib.Twofish__encblocks(
            buf, buflen, self._rk,
            self._sbox0, self._sbox1, self._sbox2, self._sbox3, o_buf
        )

    def _decblocks_c(self, buf, buflen, o_buf):
        if self._rk is None:
            self._key_schedule()
        tflib.Twofish__decblocks(
            buf, buflen, self._rk,
            self._sbox0, self._sbox1, self._sbox2, self._sbox3, o_buf
        )
