import os
import os.path
import functools
from ....core import ffi, load_library

if '__file__' in dir():
    load = functools.partial(
        load_library,
        additional_paths=[os.path.split(__file__)[0]]
    )
else:
    load = functools.partial(
        load_library,
        additional_paths=[]
    )


class BaseBlockCipher:
    __slots__ = ['_key']

    @property
    def block_size(self):
        pass

    def _get_key(self):
        return self._get_key_impl()

    def _set_key(self, value):
        self._set_key_impl(value)

    key = property(_get_key, _set_key)

    def _get_block_size(self):
        return self._get_block_size_impl()

    block_size = property(_get_block_size)

    def _get_key_impl(self):
        if self._key is None:
            return None
        return ffi.buffer(self._key)[:]

    def _set_key_impl(self, value):
        if value is None:
            self._key = None
            return
        assert isinstance(value, bytes)
        self._key = ffi.new('uint8_t[{}]'.format(len(value)), value)

    def _get_block_size_impl(self):
        pass

    def _encblocks_c(
            self,
            buf: 'cffi uint8_t*', buflen: 'int', o_buf: 'cffi uint8_t*'
            ) -> 'None':
        pass

    def encblocks(self, plain_text: 'bytes') -> 'bytes':
        assert isinstance(plain_text, bytes)
        if not plain_text:
            return b''
        assert len(plain_text) % self.block_size == 0
        plain_len = len(plain_text)
        buf = ffi.new('uint8_t[{}]'.format(plain_len), plain_text)
        self._encblocks_c(buf, plain_len, buf)
        return ffi.buffer(buf)[:]

    def _decblocks_c(
            self,
            buf: 'cffi uint8_t*', buflen: 'int', o_buf: 'cffi uint8_t*'
            ) -> 'None':
        pass

    def decblocks(self, cipher_text: 'bytes') -> 'bytes':
        assert isinstance(cipher_text, bytes)
        if not cipher_text:
            return b''
        assert len(cipher_text) % self.block_size == 0
        cipher_len = len(cipher_text)
        buf = ffi.new('uint8_t[{}]'.format(cipher_len), cipher_text)
        self._decblocks_c(buf, cipher_len, buf)
        return ffi.buffer(buf)[:]
