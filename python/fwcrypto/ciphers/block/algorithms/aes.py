from .base import ffi, load, BaseBlockCipher

ffi.cdef(r'''
void AES__key_schedule(const uint8_t *keybuf, size_t nr,
    uint32_t *o_erk, uint32_t *o_drk);
void AES__encblocks(const uint8_t *buf, size_t len,
    const uint32_t *erk, size_t nr, uint8_t *o_buf);
void AES__decblocks(const uint8_t *buf, size_t len,
    const uint32_t *drk, size_t nr, uint8_t *o_buf);
''')
aeslib = None


class AES(BaseBlockCipher):
    __slots__ = ['_erk', '_drk', '_nr']

    def __init__(self):
        global aeslib
        if aeslib is None:
            aeslib = load('aes')
        self._key = None
        self._erk = None
        self._drk = None
        self._nr = None

    def _get_block_size_impl(self):
        return 16

    def _set_key_impl(self, value):
        old_key = self._key
        BaseBlockCipher._set_key_impl(self, value)
        try:
            assert len(value) in [16, 24, 32]
        except:
            self._key = old_key
            raise
        self._erk = None
        self._drk = None
        self._nr = 10 + (len(value) // 8 - 2) * 2

    def _key_schedule(self):
        assert self._key
        self._erk = ffi.new('uint32_t[{}]'.format(self._nr*4 + 4))
        self._drk = ffi.new('uint32_t[{}]'.format(self._nr*4 + 4))
        aeslib.AES__key_schedule(self._key, self._nr, self._erk, self._drk)

    def _encblocks_c(self, buf, buflen, o_buf):
        if self._erk is None:
            self._key_schedule()
        aeslib.AES__encblocks(buf, buflen, self._erk, self._nr, o_buf)

    def _decblocks_c(self, buf, buflen, o_buf):
        if self._drk is None:
            self._key_schedule()
        aeslib.AES__decblocks(buf, buflen, self._drk, self._nr, o_buf)
