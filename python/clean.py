import os
import shutil

def clean_dir(path):
    dirs = sorted(
        filter(
            lambda item: os.path.isdir(path + os.sep + item),
            [item for item in os.listdir(path)]
        )
    )
    if '__pycache__' in dirs:
        dirs.remove('__pycache__')
        shutil.rmtree(path + os.sep + '__pycache__')
    for dir in dirs:
        clean_dir(path + os.sep + dir)

if __name__ == '__main__':
    clean_dir(os.getcwd())