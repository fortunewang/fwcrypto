import os
import shutil
import subprocess
from setuptools import setup, find_packages, Extension

subprocess.call('mingw32-make -C ../src')
shutil.copyfile('../src/lib/aes.dll','fwcrypto/ciphers/block/algorithms/aes.dll')

setup(
    name='fwcrypto',
    version='0.1',
    description='a simple cryptographic library',
    author='Fortune Wang',
    url='http://bitbucket.org/fortunewang/fwcrypto',
    license='public domain',
    packages=find_packages(),
    package_data={'':['*.dll']}
)

subprocess.call('mingw32-make -C ../src clean')
os.remove('fwcrypto/ciphers/block/algorithms/aes.dll')