import os
import base64
from fwcrypto.ciphers.block import *

if __name__ == '__main__':
    key = os.urandom(32)
    iv = os.urandom(16)
    msg = b'Veronica Decides to Die'
    print('msg:', msg)

    cipher = algorithms.AES()
    cipher.key = key
    
    print()
    print('Using nopadding and stream input:')
    
    print()
    print('CFB(2):')
    cipher = Buffered(algorithms.AES, modes.CFB, key=key, iv=iv, segment_size=2)
    enc = cipher.encrypt(msg) + cipher.finalize()
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decrypt(enc) + cipher.finalize()
    print('dec:', dec)
    
    print()
    print('OFB:')
    cipher = Buffered(algorithms.AES, modes.OFB, key=key, iv=iv)
    enc = cipher.encrypt(msg) + cipher.finalize()
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decrypt(enc) + cipher.finalize()
    print('dec:', dec)
    
    print()
    print('CTR-LE:')
    cipher = Buffered(algorithms.AES, modes.CTR, key=key, iv=iv)
    enc = cipher.encrypt(msg) + cipher.finalize()
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decrypt(enc) + cipher.finalize()
    print('dec:', dec)
    
    print()
    print('Using PKCS#7 auto-padded input:')
    
    print()
    print('ECB:')
    cipher = Buffered(algorithms.AES, modes.ECB, paddings.PKCS_7, key=key)
    enc = cipher.encrypt(msg) + cipher.finalize()
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decrypt(enc) + cipher.finalize()
    print('dec:', dec)

    print()
    print('CBC:')
    cipher = Buffered(algorithms.AES, modes.CBC, paddings.PKCS_7, key=key, iv=iv)
    enc = cipher.encrypt(msg) + cipher.finalize()
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decrypt(enc) + cipher.finalize()
    print('dec:', dec)

    print()
    print('PCBC:')
    cipher = Buffered(algorithms.AES, modes.PCBC, paddings.PKCS_7, key=key, iv=iv)
    enc = cipher.encrypt(msg) + cipher.finalize()
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decrypt(enc) + cipher.finalize()
    print('dec:', dec)
