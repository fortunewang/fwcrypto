import os
import base64

from fwcrypto.ciphers.block import *
from fwcrypto.ciphers.block.modes.ctr import LittleEndianCounter, BigEndianCounter

if __name__ == '__main__':
    key = os.urandom(32)
    iv = os.urandom(16)
    msg = b'love never keeps a man from pursuing his destiny'
    print('msg:', msg)

    cipher = algorithms.AES()
    cipher.key = key

    # ECB
    print()
    print('ECB:')
    mode_cipher = modes.ECB(cipher)
    enc = mode_cipher.encrypt(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = mode_cipher.decrypt(enc)
    print('dec:', dec)

    # CBC, OFB, PCBC
    for mode_name, mode in [
            ['CBC', modes.CBC],
            ['OFB', modes.OFB],
            ['PCBC', modes.PCBC]
            ]:
        print()
        print(mode_name + ':')
        mode_cipher = mode(cipher)
        mode_cipher.iv = iv
        enc = mode_cipher.encrypt(msg)
        mode_cipher.reset()
        print('enc:', base64.b16encode(enc).decode())
        dec = mode_cipher.decrypt(enc)
        print('dec:', dec)

    # CTR-LE
    print()
    print('CTR-LE:')
    mode_cipher = modes.CTR(cipher, LittleEndianCounter)
    mode_cipher.iv = iv
    enc = mode_cipher.encrypt(msg)
    mode_cipher.reset()
    print('enc:', base64.b16encode(enc).decode())
    dec = mode_cipher.decrypt(enc)
    print('dec:', dec)

    # CTR-BE
    print()
    print('CTR-BE:')
    mode_cipher = modes.CTR(cipher, BigEndianCounter)
    mode_cipher.iv = iv
    enc = mode_cipher.encrypt(msg)
    mode_cipher.reset()
    print('enc:', base64.b16encode(enc).decode())
    dec = mode_cipher.decrypt(enc)
    print('dec:', dec)

    # CFB(4)
    print()
    print('CFB(4):')
    mode_cipher = modes.CFB(cipher, 4)
    mode_cipher.iv = iv
    enc = mode_cipher.encrypt(msg)
    mode_cipher.reset()
    print('enc:', base64.b16encode(enc).decode())
    dec = mode_cipher.decrypt(enc)
    print('dec:', dec)
