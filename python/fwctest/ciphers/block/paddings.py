from fwcrypto.ciphers.block.paddings import *

if __name__ == '__main__':
    msg = b'hello world'
    print('original:',msg)
    print()
    
    print('method:   NoPadding')
    padded = NoPadding.pad(msg,16)
    print('padded:  ',padded)
    unpadded = NoPadding.unpad(padded,16)
    print('unpadded:',unpadded)
    print()
    
    print('method:   ANSI X.923')
    padded = ANSI_X_923.pad(msg,16)
    print('padded:  ',padded)
    unpadded = ANSI_X_923.unpad(padded,16)
    print('unpadded:',unpadded)
    print()
    
    print('method:   ISO 10126')
    padded = ISO_10126.pad(msg,16)
    print('padded:  ',padded)
    unpadded = ISO_10126.unpad(padded,16)
    print('unpadded:',unpadded)
    print()
    
    print('method:   PKCS#7')
    padded = PKCS_7.pad(msg,16)
    print('padded:  ',padded)
    unpadded = PKCS_7.unpad(padded,16)
    print('unpadded:',unpadded)
    print()
    
    print('method:   ISO/IEC 7816-4')
    padded = ISO_IEC_7816_4.pad(msg,16)
    print('padded:  ',padded)
    unpadded = ISO_IEC_7816_4.unpad(padded,16)
    print('unpadded:',unpadded)
    print()