import os
import base64
from fwcrypto.ciphers.block import *

if __name__ == '__main__':
    msg = b'love never keeps a man from pursuing his destiny'
    print('msg:', msg)
    
    # AES-128
    print()
    print('AES-128:')
    cipher = algorithms.AES()
    cipher.key = os.urandom(16)
    enc = cipher.encblocks(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decblocks(enc)
    print('dec:', dec)
    
    # AES-192
    print()
    print('AES-192:')
    cipher = algorithms.AES()
    cipher.key = os.urandom(24)
    enc = cipher.encblocks(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decblocks(enc)
    print('dec:', dec)
    
    # AES-256
    print()
    print('AES-256:')
    cipher = algorithms.AES()
    cipher.key = os.urandom(32)
    enc = cipher.encblocks(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decblocks(enc)
    print('dec:', dec)
    
    # Twofish-128
    print()
    print('Twofish-128:')
    cipher = algorithms.Twofish()
    cipher.key = os.urandom(16)
    enc = cipher.encblocks(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decblocks(enc)
    print('dec:', dec)
    
    # Twofish-192
    print()
    print('Twofish-192:')
    cipher = algorithms.Twofish()
    cipher.key = os.urandom(24)
    enc = cipher.encblocks(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decblocks(enc)
    print('dec:', dec)
    
    # Twofish-256
    print()
    print('Twofish-256:')
    cipher = algorithms.Twofish()
    cipher.key = os.urandom(32)
    enc = cipher.encblocks(msg)
    print('enc:', base64.b16encode(enc).decode())
    dec = cipher.decblocks(enc)
    print('dec:', dec)
