#include "fwcrypto/ciphers.h"

namespace fwcrypto {

vector<uint8_t> BaseBlockCipher::encblocks(uint8_t *plain_text, size_t len)
{
    if(len == 0)
        return vector<uint8_t>();
    else if(len % block_size() != 0)
        throw invalid_argument("Data must be a multiple of blocks.");
    vector<uint8_t> buf(len);
    this->_encblocks_c(plain_text, len, buf.data());
    return buf;
}

vector<uint8_t> BaseBlockCipher::decblocks(uint8_t *cipher_text, size_t len)
{
    if(len == 0)
        return vector<uint8_t>();
    else if(len % block_size() != 0)
        throw invalid_argument("Data must be a multiple of blocks.");
    vector<uint8_t> buf(len);
    this->_decblocks_c(cipher_text, len, buf.data());
    return buf;
}

} // namespace fwcrypto