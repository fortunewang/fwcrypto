#include "fwcrypto/ciphers.h"

namespace fwcrypto {

extern "C" void AES__key_schedule(const uint8_t *keybuf, size_t nr,
    uint32_t *o_erk, uint32_t *o_drk);
extern "C" void AES__encblocks(const uint8_t *buf, size_t len,
    const uint32_t *erk, size_t nr, uint8_t *o_buf);
extern "C" void AES__decblocks(const uint8_t *buf, size_t len,
    const uint32_t *drk, size_t nr, uint8_t *o_buf);

void AES::set_key(uint8_t *value, size_t len)
{
    if(len == 0)
    {
        m_key.clear();
        return;
    }
    if(len != 16 && len !=24 && len != 32)
        throw invalid_argument("Key length error.");
    mm_nr = 10 + (len / 8 - 2) * 2;
    m_key.assign(value, len);
    m_erk.clear();
    m_drk.clear();
}

vector<uint8_t> AES::get_key() const
{
    return m_key;
}

void AES::_key_schedule()
{
    if(m_key.empty())
        throw logic_error("Key must be set before encryption/decryption.");
    m_erk.resize(m_nr * 4 + 4);
    m_drk.resize(m_nr * 4 + 4);
    AES__key_schedule(m_key.data(), m_nr, m_erk.data(), m_drk.data());
}

void AES::_encblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf)
{
    if(m_erk.empty())
        _key_schedule();
    AES__encblocks(buf, len, m_erk.data(), m_nr, o_buf);
}

void AES::_decblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf)
{
    if(m_drk.empty())
        _key_schedule();
    AES__decblocks(buf, len, m_drk.data(), m_nr, o_buf);
}

} // namespace fwcrypto