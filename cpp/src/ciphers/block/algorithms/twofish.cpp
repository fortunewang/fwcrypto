#include "fwcrypto/ciphers.h"

namespace fwcrypto {

extern "C" void Twofish__key_schedule(const uint8_t *keybuf, size_t nr,
    uint32_t *o_erk, uint32_t *o_drk);
extern "C" void Twofish__encblocks(const uint8_t *buf, size_t len,
    const uint32_t *erk, size_t nr, uint8_t *o_buf);
extern "C" void Twofish__decblocks(const uint8_t *buf, size_t len,
    const uint32_t *drk, size_t nr, uint8_t *o_buf);

void Twofish::set_key(uint8_t *value, size_t len)
{
    if(len == 0)
    {
        m_key.clear();
        return;
    }
    if(len != 16 && len !=24 && len != 32)
        throw invalid_argument("Key length error.");
    m_key.assign(value, len);
    m_rk.clear();
}

vector<uint8_t> Twofish::get_key() const
{
    return m_key;
}

void Twofish::_key_schedule()
{
    if(m_key.empty())
        throw logic_error("Key must be set before encryption/decryption.");
    m_rk.resize(40);
    Twofish__key_schedule(m_key.data(), m_key.size(), m_rk.data(),
        m_sbox0, m_sbox1, m_sbox2, m_sbox3);
}

void Twofish::_encblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf)
{
    if(m_rk.empty())
        _key_schedule();
    Twofish__encblocks(buf, len, m_rk.data(),
        m_sbox0, m_sbox1, m_sbox2, m_sbox3, o_buf);
}

void Twofish::_decblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf)
{
    if(m_rk.empty())
        _key_schedule();
    Twofish__decblocks(buf, len, m_rk.data(),
        m_sbox0, m_sbox1, m_sbox2, m_sbox3, o_buf);
}

} // namespace fwcrypto