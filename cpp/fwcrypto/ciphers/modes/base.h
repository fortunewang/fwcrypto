#ifndef _FWCRYPTO_CIPHERS_BLOCK_MODES_BASE_
#define _FWCRYPTO_CIPHERS_BLOCK_MODES_BASE_

namespace fwcrypto {

class BaseBlockCipherMode
{
public:
    virtual encrypt(const uint8_t *plain_text, size_t len) = 0;
    virtual decrypt(const uint8_t *cipher_text, size_t len) = 0;
};

class BlockCipherModeHasIV: public BaseBlockCipherMode
{
public:
    vector<uint8_t> get_iv() const noexcept;
    void set_iv(const uint8_t *value, size_t len);
    void reset() noexcept;
};

} // namespace fwcrypto

#endif // _FWCRYPTO_CIPHERS_BLOCK_MODES_BASE_