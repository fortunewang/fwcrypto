#ifndef _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_AES_
#define _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_AES_

#include "base.h"

namespace fwcrypto {

class AES: public BaseBlockCipher
{
public:
    virtual size_t block_size() const noexcept { return 16; }
    virtual void set_key(uint8_t *value, size_t len);
    virtual vector<uint8_t> get_key() const;
    virtual void _encblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf);
    virtual void _decblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf);
private:
    void _key_schedule();
    size_t m_nr;
    vector<uint8_t> m_key;
    vector<uint32_t> m_erk,m_drk;
};

} // namespace fwcrypto

#endif // _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_AES_