#ifndef _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_BASE_
#define _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_BASE_

namespace fwcrypto {

class BaseBlockCipher
{
public:
    virtual size_t block_size() const noexcept = 0;
    virtual void set_key(uint8_t *value, size_t len) = 0;
    virtual vector<uint8_t> get_key() const = 0;
    virtual void _encblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf) = 0;
    virtual vector<uint8_t> encblocks(uint8_t *plain_text, size_t len);
    virtual void _decblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf) = 0;
    virtual vector<uint8_t> decblocks(uint8_t *cipher_text, size_t len);
};

} // namespace fwcrypto

#endif // _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_BASE_