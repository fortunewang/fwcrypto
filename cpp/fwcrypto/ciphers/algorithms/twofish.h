#ifndef _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_TWOFISH_
#define _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_TWOFISH_

#include "base.h"

namespace fwcrypto {

class Twofish: public BaseBlockCipher
{
public:
    virtual size_t block_size() const noexcept { return 16; }
    virtual void set_key(uint8_t *value, size_t len);
    virtual vector<uint8_t> get_key() const;
    virtual void _encblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf);
    virtual void _decblocks_c(uint8_t *buf, size_t len, uint8_t *o_buf);
private:
    void _key_schedule();
    vector<uint8_t> m_key;
    vector<uint32_t> m_rk;
    uint32_t m_sbox0[256], m_sbox1[256], m_sbox2[256], m_sbox3[256];
};

} // namespace fwcrypto

#endif // _FWCRYPTO_CIPHERS_BLOCK_ALGORITHMS_TWOFISH_